var app = {};

(function(){
	/* DEFINITIONS */
	//var app = {}

	app.settings = {
		size: {
			width: 32,
			height: 24
		}
	}

	app.history = {
		index: -1,
		MAX_LENGTH: 10,
		snaps: [],
		undo: function()
			{
				app.canvas.selection.flush()

				if (app.history.index > 0){
					app.history.index--
					app.history.set()
				}
			},
		redo: function()
			{
				if (app.history.index < app.history.snaps.length - 1 && app.history.snaps.length > 0){
					app.history.index++
					app.history.set()
				}
			},
		fetch: function()
			{
				var res = app.canvas.loop(function(x,y,cell){			
					return {
						color: cell.getAttribute('color-byte'),
						className: cell.className
					}
				},'array')

				return res
			},
		set: function()
			{
				var data = app.history.snaps[app.history.index]

				app.canvas.loop(function(x,y,cell,index){
					cell.setAttribute('color-byte',data[index].color)
					cell.className = data[index].className
				})

				app.preview.update()
			},
		add: function()
			{
				if (app.history.index === app.history.snaps.length - 1 && app.history.snaps.length < app.history.MAX_LENGTH){
					app.history.index++
					app.history.snaps.push(app.history.fetch())
				} else if (app.history.index === app.history.MAX_LENGTH) {
					var i

					for (i = 0; i < app.history.MAX_LENGTH; i++){
						app.history.snaps[i] = app.history.snaps[i + 1]
					}

					app.history.snaps[app.history.MAX_LENGTH] = app.history.fetch()
				} else {
					app.history.snaps.splice(app.history.index + 1)
					app.history.snaps.push(app.history.fetch())
					app.history.index = app.history.snaps.length - 1
				}
			},
		init: function()
			{
				document.addEventListener('keypress',function(e){
					if (e.ctrlKey){
						;((e.shiftKey && e.keyCode === 26) || e.keyCode === 25)
							? app.history.redo()
							: (e.keyCode === 26) && (app.history.undo())
					}
				})

				app.history.add()
			}
	}

	app.dom = {
		init: function()
			{
				app.$ = {}

				//Colors
				app.$.brushes = document.querySelectorAll('.app-brush')
				app.$.palette = document.querySelector('.app-palette')
				app.$.sort = document.querySelectorAll('.app-sort-trigger')
				app.$.reverse = document.querySelector('.app-reverse input')

				//Mode
				app.$.mode = document.querySelector('.app-mode input')
				app.$.grid = document.querySelector('.app-grid input')
				app.$.snap = document.querySelector('.app-snap input')
				app.$.hideColors = document.querySelector('.app-palette-mode')

				//Canvas
				app.$.canvas = document.querySelector('.app-canvas')
				app.$.canvas.cells = document.querySelectorAll('.app-canvas td')
				app.$.preview = document.querySelector('.app-preview')

				//Toolbar
				app.$.toolbar = {
					load: document.querySelector('.app-btn-load'),
					exportImage: document.querySelector('.app-btn-export-img'),
					exportFull: document.querySelector('.app-btn-export-full'),
					importImage: document.querySelector('.app-btn-import-image'),
					importTap: document.querySelector('.app-btn-import-tap'),
					exportTap: document.querySelector('.app-btn-export-tap'),
					importAttr: document.querySelector('.app-btn-import-attr'),
					exportAttr: document.querySelector('.app-btn-export-attr'),
					exportScr: document.querySelector('.app-btn-export-scr'),
					clear: document.querySelector('.app-btn-clear'),
					clearIndicator: document.querySelector('.app-btn-clear span')
				}

				app.$.rorg = document.querySelector('.app-rorg')
				app.$.rorg.compo = app.$.rorg.querySelector('.compo-name')
				app.$.rorg.event = app.$.rorg.querySelector('.compo-event')
				app.$.rorg.send = app.$.rorg.querySelector('.app-btn-send')

				app.$.dropdowns = document.querySelectorAll('.btn-dropdown')

				app.$.paletteSelector = document.querySelector('.app-palette-select')
				app.$.cssPalette = document.getElementById('css-palette')

				//Tools
				app.$.tools = {
					brush: document.querySelector('.app-tool-brush'),
					select: document.querySelector('.app-tool-select')
				}

				//Windos
				app.$.windows = document.querySelectorAll('.app-window')

				//Info
				app.$.position = {
					x: document.querySelector('.app-pos-x'),
					y: document.querySelector('.app-pos-y'),
				}

				//Dialogs

				app.$.dialog = document.querySelector('.app-dialog')
				app.$.dialog.close = app.$.dialog.querySelector('.dialog-close')
				app.$.dialog.body = app.$.dialog.querySelector('.dialog-body')
				app.$.dialog.buttons = app.$.dialog.querySelectorAll('.btn')
				app.$.dialog.head = app.$.dialog.querySelector('.dialog-title')
			}
	}

	app.polyfills = function()
		{
			if (!HTMLCanvasElement.prototype.toBlob) {
				Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
					value: function (callback, type, quality) {
						var binStr = atob( this.toDataURL(type, quality).split(',')[1] ),
							len = binStr.length,
							arr = new Uint8Array(len);

						for (var i=0; i<len; i++ ) {
							arr[i] = binStr.charCodeAt(i);
						}

						callback( new Blob( [arr], {type: type || 'image/png'} ) );
					}
				});
			}
		}

	app.dialog = {
		buttons:
			{
				OK: 1,
				CANCEL: 2,
				YES: 4,
				NO: 8,
				OK_CANCEL: 3,
				YES_NO: 12 
			},
		show: function(title,content,buttons,callback)
			{
				app.$.dialog.head.innerHTML = title
				app.$.dialog.body.innerHTML = ''
				;(content instanceof HTMLElement) ? app.$.dialog.body.appendChild(content) : app.$.dialog.body.innerHTML = content
				
				app.dialog.process(buttons,callback)

				app.$.dialog.style.display = 'block'
			},
		process: function(buttons,callback)
			{
				var i, btn, clone, nbuttons = []

				for (i = 0; i < 4; i++){
					btn = app.$.dialog.buttons[i]
					clone = btn.cloneNode()

					while (btn.firstChild) {
					  clone.appendChild(btn.lastChild);
					}

					clone.style.display = (buttons & (1 << i)) ? 'inline-block' : 'none'
					clone.addEventListener('click',app.dialog.handler(callback,(1 << i)))

					nbuttons.push(clone)
					btn.parentNode.appendChild(clone)
					btn.parentNode.removeChild(btn)
				}

				app.$.dialog.buttons = nbuttons
			},
		handler: function(callback,result)
			{
				return function(e)
				{
					e.preventDefault()
					callback(result,app.$.dialog.body)

					;(result === 2) && app.dialog.hide()
				}
			},
		hide: function(e)
			{
				e && e.preventDefault
				app.$.dialog.style.display = 'none'
			},
		button: function(name)
			{
				var btn = app.dialog.buttons[name.toUpperCase()]
				return (btn !== 8)
					? app.$.dialog.buttons[btn >> 1]
					: app.$.dialog.buttons[3]
			},
		init: function()
			{
				app.$.dialog.close.addEventListener('click',app.dialog.hide)
			}
	}

	app.rorg = {
		formname: 'rorgForm',
		api:
			{
				url: 'http://events.retroscene.org/api/',
				req: function(url, type, callback, data)
					{
						/* const */var xobj = new XMLHttpRequest()

						url = (url.indexOf('?') > -1)
							? url + '&ResponseType=json'
							: url + '?ResponseType=json'

						if (type === 'GET'){
				            xobj.overrideMimeType("application/json")
						}

						xobj.withCredentials = true

			            xobj.open(type, url, true)

			            console.log('Fetching: ', url)

			            xobj.onreadystatechange = function () 
			                {
			                    if (xobj.readyState == 4) 
			                    {
			                    	if (xobj.status == "200")
			                    		{
			                        		callback(xobj.responseText)
			                    		}
			                		else
			                			{
			                    			console.error(xobj.statusText + ": " + xobj.responseURL)
			                    			callback(JSON.stringify({
			                    				Status: 'error',
			                    				error: xobj.statusText
			                    			}))
			                    		}
			                    }
			                }

			            xobj.send(data)
					},
				get: function(url, callback)
					{
						app.rorg.api.req(url, 'GET', callback, null)
					},
				post: function(url, data, callback)
					{
						app.rorg.api.req(url, 'POST', callback, data)
					},
				send: function(title, author, callback)
					{
						var formData = new FormData(),
							blob = new Blob([decodeURIComponent(app.files.process.attr.get())], { type: 'application/octet-stream'}),
							file = new File([blob], '53c.atr')

						formData.append("Title", title)
						formData.append("Author", author)
						formData.append("file53c", file)

						console.log('File:', file)

						app.rorg.api.post(app.rorg.api.url + 'competitions/upload53c', formData, callback)
					},
				status: function(callback)
					{
						app.rorg.api.get(app.rorg.api.url + 'competitions/get53c',function(data){
							data = JSON.parse(data)

							if (data.Status === 'success'){
								;(data.Competition.ReceptionAvailable === '1')
									? callback(data)
									: app.rorg.state('offline')
							} else {
								console.error('Events API error:', data.Message)
								app.rorg.state('offline')
							}
						})
					}
			},
		online: function(info)
			{
				app.rorg.info = info
				app.$.rorg.compo.textContent = info.Competition.Title
				app.$.rorg.event.textContent = info.Competition.EventTitle
				app.rorg.state('online')
			},
		state: function(state)
			{
				app.$.rorg.className = 'app-rorg rorg-compo ' + state
			},
		form: function()
			{
				var f = document.createElement('form')

				f.setAttribute('name',app.rorg.formname)

				f.innerHTML = '<label for="worktitle">Title:</label>\
				<input type="text" id="worktitle" name="Title" value="" />\
				<label for="cusername">Author:</label>\
				<input type="text" id="cusername" name="Author" value="' + app.rorg.info.Username + '"' + ((app.rorg.info.IsGuest === "1") ? '' : 'disabled') + ' />'

				return f 
			},
		dialog: function(e)
			{
				e && e.preventDefault()

				app.dialog.show(
					'Send pixels to «' + app.rorg.info.Competition.Title + '»',
					app.rorg.form(),
					app.dialog.buttons.OK_CANCEL,
					app.rorg.onsend
				)
			},
		onsend: function(result)
			{
				if (result === 1 && app.rorg.check()){
					app.rorg.api.send(
						document.getElementById('worktitle').value,
						document.getElementById('cusername').value, 
						function(res){
							res = JSON.parse(res)

							if (res.Status === 'success'){
								app.dialog.show('Работа отправлена', '<p>Спасибо за участие в ' + app.rorg.info.Competition.EventTitle + '!', app.dialog.buttons.OK, function(){
									app.dialog.hide()
									app.rorg.state('offline')
								})
							} else {
								console.error('Events API error:', res.Message)
							}
						}
					)
				}
			},
		check: function()
			{
				var t = document.getElementById('worktitle'),
					u = document.getElementById('cusername')

				return app.rorg.checkField(t) && app.rorg.checkField(u)
			},
		checkField: function(input)
			{
				input.className = input.className.replace(' error','')
				input.setAttribute('placeholder','')

				if (!input.value){
					input.className += ' error'
					input.setAttribute('placeholder','Please fill this field')
					return false
				}

				return true
			},
		init: function()
			{
				app.$.rorg.send.addEventListener('click',app.rorg.dialog)
				app.rorg.state('status')
				app.rorg.api.status(app.rorg.online)
			}
	}

	app.windows = {
		positions: function()
			{
				var i, res = {}

				for (i = 0; i < app.$.windows.length; i++){
					res[app.$.windows[i].id] = [
						app.$.windows[i].offsetLeft,
						app.$.windows[i].offsetTop
					]
				}

				return res
			},
		apply: function(h)
			{
				h.addEventListener('mousedown',function(e){
					this.setAttribute('offset','[' + (e.clientX - this.parentNode.offsetLeft) + ',' + (e.clientY - this.parentNode.offsetTop) + ']')
				})

				document.body.addEventListener('mousemove',function(e){
					var offset = JSON.parse(h.getAttribute('offset'))

					if (offset){
						h.parentNode.style.left = e.clientX - offset[0] + "px"
						h.parentNode.style.top = e.clientY - offset[1] + 'px'
					}
				})

				h.addEventListener('touchmove',function(e){
				    var touch = e.targetTouches[0]
				    e.preventDefault();
				    
				    this.parentNode.style.left = touch.pageX + 'px';
				    this.parentNode.style.top = Math.max(0,touch.pageY) + 'px';

				    app.storage.settings.save()
				})

				h.addEventListener('mouseup',function(e){
					this.removeAttribute('offset')
					app.storage.settings.save()
				})
			},
		init: function()
			{
				var i,h

				for (i = 0; i < app.$.windows.length; i++){
					app.windows.apply(app.$.windows[i].querySelector('.app-window-header'))
					app.$.windows[i].onselectstart=function(){ return false }
				}
			}
	}

	app.tools = {
		color:
			{
				nearest:  function(needle) 
					{
					    var distance,
					        minDistance = Infinity,
					        rgb,
					        value,
					        colors = app.palette.HALFTONE

					    if (!needle) {
					      return null;
					    }

					    for (var i = 0; i < colors.length; ++i) {
					      rgb = colors[i].rgb

					      distance = Math.sqrt(
					        Math.pow(needle.r - rgb.r, 2) +
					        Math.pow(needle.g - rgb.g, 2) +
					        Math.pow(needle.b - rgb.b, 2)
					      )

					      if (distance < minDistance) {
					        minDistance = distance
					        value = colors[i]
					      }
					    }

					    return value
					  }
			}
	}

	app.info = {
		position: function(x,y)
			{
				app.$.position.x.innerText = x
				app.$.position.y.innerText = y
			}
	}

	app.storage = {
		save: function(name,data)
			{
				localStorage.setItem(name,JSON.stringify(data))
			},
		load: function(name)
			{
				return JSON.parse(localStorage.getItem(name))
			},
		settings:
			{
				save: function()
					{
						app.storage.save('s-mode',app.mode.current())
						app.storage.save('s-grid',app.mode.grid.on())
						app.storage.save('s-snap',app.mode.grid.snap)
						app.storage.save('s-palette-mode',app.mode.hideColors)

						app.storage.save('s-reverse',app.palette.sort.reverse)
						app.storage.save('s-sort',app.palette.sort.mode)

						app.storage.save('s-brushes',app.brushes.current)
						app.storage.save('s-palette',app.$.paletteSelector.value)

						app.storage.save('s-windows',JSON.stringify(app.windows.positions()))
						app.storage.save('s-preview-zoom',Math.floor(parseInt(app.$.preview.style.width) / app.settings.size.width))
					},
				load: function()
					{
						var i, pos, w, zoom

						app.mode.set(app.storage.load('s-mode') || 'color')

						zoom = app.storage.load('s-preview-zoom') || 2
						app.$.preview.style.width = app.settings.size.width * zoom + 'px'
						app.$.preview.style.height = app.settings.size.height * zoom + 'px'

						app.mode.grid.snap = app.storage.load('s-snap') || false
						app.$.snap.checked = app.storage.load('s-snap') || false

						app.mode.grid.trigger(app.storage.load('s-grid') || false)
						app.$.grid.checked = app.storage.load('s-grid') || false

						app.palette.sort.reverse = app.storage.load('s-reverse') || false
						app.$.reverse.checked = app.palette.sort.reverse
						app.palette.sort.mode = app.storage.load('s-sort') || 'rgb'
						document.getElementById('app-sort-' + app.palette.sort.mode).checked = true
						app.palette.sort[app.palette.sort.mode]()
						app.palette.redraw()

						app.$.paletteSelector.querySelector('option[value="' + (app.storage.load('s-palette') || 'pulsar') + '"]').selected = true
						app.$.cssPalette.href = '/assets/styles/palette/' + app.$.paletteSelector.value + '.css'

						app.brushes.current = app.storage.load('s-brushes') || [
							app.palette.search('c-0x0'),
							app.palette.search('c-0x0'),
							app.palette.search('c-0x0')
						]
						
						for (i = 0; i < app.brushes.current.length; i++){
							app.brushes.select(i,app.brushes.current[i])
						}

						pos = JSON.parse(app.storage.load('s-windows')) || {}
						for (i in pos){
							w = document.getElementById(i)

							w.style.left = Math.max(0,pos[i][0]) + 'px'
							w.style.top = Math.max(0,pos[i][1]) + 'px'
						}

						app.mode.hideColors = app.storage.load('s-palette-mode')
						app.$.hideColors.checked = app.mode.hideColors
						app.mode.palette(app.mode.hideColors)
					}
			},
		draft:
			{
				save: function()
					{
						var x,y,
							data = []

						for (y = 0; y < app.settings.size.height; y++){
							data[y] = []

							for (x = 0; x < app.settings.size.width; x++){
								data[y][x] = app.canvas.getColor(x,y)
							}
						}

						app.storage.save('draft',data)
					},
				load: function()
					{
						var x,y,
							data = app.storage.load('draft')

						if (data !== null){
							app.canvas.loop(function(x,y,cell){
								app.canvas.setColor(x,y,data[y][x])
							})
						}

						app.history.add()
						app.preview.update()
					}
			}
	}

	app.files = {
		data:
			{
				tap:
					{
						loader: '%13%00%00%00%63%68%75%6e%6b%79%20%20%20%20%7e%00%0a%00%7e%00%08%80%00%ff%00%00%1c%00%ea%21%00%40%11%01%40%36%55%01%00%01%ed%b0%36%aa%0d%ed%b0%21%00%40%06%16%ed%b0%c9%0d%00%0a%10%00%ef%22%22%af%32%32%35%32%38%0e%00%00%00%58%00%0d%00%14%39%00%e7%30%0e%00%00%00%00%00%3a%f9%c0%28%be%32%33%36%33%35%0e%00%00%53%5c%00%2b%32%35%36%0e%00%00%00%01%00%2a%be%32%33%36%33%36%0e%00%00%54%5c%00%2b%35%0e%00%00%05%00%00%29%0d%00%1e%09%00%f2%30%0e%00%00%00%00%00%0d%1f',
						header: '%13%00%00%03%73%63%72%65%65%6e%20%20%20%20%00%03%00%58%00%80%d4',
						dataStart: '%02%03%ff'
					}
			},
		process:
			{
				attr:
					{
						load: function(fr,$status,callback)
							{
								return function()
									{
										var data = window.atob(fr.result.split(',')[1])

										app.canvas.loop(function(x,y,cell,index){
											if (index < data.length){
												color = data.charCodeAt(index)
												if (app.palette.classByColor(color) === ' c-0x0' && color !== 0){
													cell.setAttribute('color-byte',app.tools.color.nearest(app.palette.rgbByColor(color)).color)
													cell.className = app.tools.color.nearest(app.palette.rgbByColor(color)).className
												} else {
													cell.setAttribute('color-byte',color)
													cell.className = app.palette.classByColor(color)
												}
											}
										})

										callback()
									}
							},
						get: function()
							{
								var data = '',
									hexDigits = '0123456789abcdef'

								data = app.canvas.loop(function(x,y,cell){
									byte = cell.getAttribute('color-byte')
									return ( '%' + hexDigits.substr(byte >> 4,1) + hexDigits.substr(byte & 0x0f,1) )
								},'string',data)

								return data
							}
					},
				tap:
					{
						load: function(fr,$status,callback)
							{
								return function()
									{
										var totalHlength = app.files.data.tap.loader.length / 3 + app.files.data.tap.header.length / 3  + app.files.data.tap.dataStart.length / 3,
											data = window.atob(fr.result.split(',')[1])
													.substr(totalHlength),
											color

										app.canvas.loop(function(x,y,cell,index){
											if (index < data.length){
												color = data.charCodeAt(index)
												if (app.palette.classByColor(color) === ' c-0x0' && color !== 0){
													cell.setAttribute('color-byte',app.tools.color.nearest(app.palette.rgbByColor(color)).color)
													cell.className = app.tools.color.nearest(app.palette.rgbByColor(color)).className
												} else {
													cell.setAttribute('color-byte',color)
													cell.className = app.palette.classByColor(color)
												}
											}
										})

										callback()
									}
							},
						get: function()
							{
								var loader = app.files.data.tap.loader,
									header = app.files.data.tap.header,
									data = app.files.data.tap.dataStart,
									checksum = 0xff,
									hexDigits = '0123456789abcdef',
									y,x,byte,checksumString

								data = app.canvas.loop(function(x,y,cell){
									byte = cell.getAttribute('color-byte')
									checksum = checksum ^ byte
									return ( '%' + hexDigits.substr(byte >> 4,1) + hexDigits.substr(byte & 0x0f,1) )
								},'string',data)

								checksumString = '%' + hexDigits.substr(checksum >> 4,1) + hexDigits.substr(checksum & 0x0f,1);
								return loader + header + data + checksumString
							}
					},
				scr:
					{
						get: function()
							{
								var byte, color, x, line, block, third, 
									hexDigits = '0123456789abcdef',
									size = 8,
									clear = 0,
									chunk = 170, //or 85
									data = ''

								for (third = 0; third < 3; third++){
									for (block = 0; block < size; block++){
										for (line = 0; line < size; line++){
											for (x = 0; x < app.settings.size.width; x++){
												color = app.$.canvas.cells[third * app.settings.size.width * 8 + line * app.settings.size.width + x].getAttribute('color-byte')
												byte = (block % 2) ? '%aa' : '%55'
												data += app.palette.isSolid(color) ? '%00' : byte
											}
										}
									}
								}

								data += app.canvas.loop(function(x,y,cell,index){
									byte = cell.getAttribute('color-byte')
									return ( '%' + hexDigits.substr(byte >> 4,1) + hexDigits.substr(byte & 0x0f,1) )

								})

								return data
							}
					},
				image:
					{
						load: function(fr,$status,callback)
							{
								return function()
									{
										var img = new Image()

										img.style.maxWidth = '32px'
										img.style.maxHeight = '24px'

										$status.innerHTML = '<span class="spinner-inline"></span> Processing...<br/>'
										$status.appendChild(img)
										img.onload = app.files.process.image.create(callback)
										img.src = fr.result;
									}
							},
						create: function(callback)
							{
								return function(e)
									{
										var canvas = document.createElement('canvas'),
											ctx = canvas.getContext('2d'),
											x,y,cd

										canvas.width = 32
										canvas.height = 24

										ctx.drawImage(this,0,0,32,24)

										for (y = 0; y < 24; y++){
											for (x = 0; x < 32; x++){
												cd = ctx.getImageData(x,y,1,1).data
												app.canvas.setColor(x,y,app.tools.color.nearest({
													r: cd[0],
													g: cd[1],
													b: cd[2]
												}).color)
											}
										}

										callback()
									}
							}
					}
			},
		imp:
			{
				dialog: function(title, text, accept, handler)
					{
						var form = document.createElement('form'),
							innerForm = '<label>' + text + '</label><input type="file" name="import-file" accept="' + accept + '" /><br/><p class="error-text">Please select a file</p>'
							callback = function(result,$body){
								if (result === 1){
									if ($body.querySelector('input').files[0]){
										handler($body.querySelector('input').files[0],form,function(){
											app.dialog.hide()
											app.preview.update()
										})
									} else {
										form.className = 'error'
									}
								}
							}
						
						form.innerHTML = innerForm
						form.querySelector('input').addEventListener('change',function(e){
							(this.files[0]) && (form.className = '')
						})
						
						app.dialog.show(title,form,app.dialog.buttons.OK_CANCEL,callback)
					},
				image: function(file,$status,ondone)
					{
						var fr = new FileReader()

						$status.innerHTML = '<span class="spinner-inline"></span> Uploading...'
						fr.onload = app.files.process.image.load(fr,$status,ondone)

						fr.readAsDataURL(file)
					},
				tap: function(file, $status, ondone)
					{
						var fr = new FileReader()

						$status.innerHTML = '<span class="spinner-inline"></span> Uploading...'
						fr.onload = app.files.process.tap.load(fr,$status,ondone)

						fr.readAsDataURL(file)
					},
				attr: function(file, $status, ondone)
					{
						var fr = new FileReader()

						$status.innerHTML = '<span class="spinner-inline"></span> Uploading...'
						fr.onload = app.files.process.attr.load(fr,$status,ondone)

						fr.readAsDataURL(file)
					}
			},
		exp:
			{
				file: function(data,type,filename)
					{
						var blob

						if (window.navigator.msSaveOrOpenBlob) {
							var raw = []
							for ( var i = 1, len = data.length; i < len; i+=3 ) 
								{
									raw.push(parseInt(data.substr(i, 2), 16))
								}

							blob = new Blob([new Uint8Array(raw)])

							window.navigator.msSaveBlob(blob, filename)
						} else {
							var a = window.document.createElement("a")
    						blob = new Blob([data])
    						
    						;(type === 'application/octet-stream')
    							? a.href = 'data:' + type + ',' + data
    							: a.href = window.URL.createObjectURL(data, {type: type})
    						a.download = filename

						    document.body.appendChild(a)
						    a.click()
						    document.body.removeChild(a)
						}
					},
				scr: function()
					{
						app.files.exp.file(app.files.process.scr.get(),'application/octet-stream','53cExport.scr')
					},
				attr: function()
					{
						app.files.exp.file(app.files.process.attr.get(),'application/octet-stream','53cExport.atr')
					},
				tap: function()
					{
						app.files.exp.file(app.files.process.tap.get(),'application/octet-stream','53cExport.tap')
					},
				image: function(callback)
					{
						app.$.preview.toBlob(function(blob){
							app.files.exp.file(blob,'image/png;base64','53cDrawing.png')
							callback && callback()
						})
					}
			}
	}

	app.toolbar = {
		actions:
			{
				load: function()
					{
						app.storage.draft.load()
					},
				importImage: function()
					{
						app.files.imp.dialog('Upload an image', 'Select a file:', 'image/jpeg,image/png,image/gif',app.files.imp.image)
					},
				exportImage: function(callback)
					{
						app.files.exp.image(callback)
					},
				exportFull: function()
					{
						var orig = app.$.preview.toDataURL(),
							img = new Image
						
						img.src = orig;

						app.preview.context.canvas.width = app.settings.size.width * 16
						app.preview.context.canvas.height = app.settings.size.height * 16
						app.preview.context.imageSmoothingEnabled = false

						app.preview.context.drawImage(img,0,0,app.preview.context.canvas.width,app.preview.context.canvas.height)

						app.toolbar.actions.exportImage(function(){
							app.preview.context.canvas.width = app.settings.size.width
							app.preview.context.canvas.height = app.settings.size.height							
							app.preview.context.drawImage(img,0,0,app.preview.context.canvas.width,app.preview.context.canvas.height)
						})
					},
				importTap: function()
					{
						app.files.imp.dialog('Upload a TAP','Select a file:','.tap',app.files.imp.tap)
					},
				exportTap: function()
					{
						app.files.exp.tap()
					},
				importAttr: function()
					{
						app.files.imp.dialog('Upload an atr','Select a file:','.atr',app.files.imp.attr)
					},
				exportAttr: function()
					{
						app.files.exp.attr()
					},
				exportScr: function()
					{
						app.files.exp.scr()
					},
				clear: function()
					{
						var i,x,y

						if (app.toolbar.tools.current === 'select'){
							for (y = app.canvas.selection.s()[1]; y <= app.canvas.selection.e()[1]; y++){
								for (x = app.canvas.selection.s()[0]; x <= app.canvas.selection.e()[0]; x++){
									app.$.canvas.cells[y * app.settings.size.width + x].className = app.brushes.current[0].className
									app.$.canvas.cells[y * app.settings.size.width + x].setAttribute('color-byte',app.brushes.current[0].color)
								}
							}
						} else {
							for (i = 0; i < app.$.canvas.cells.length; i++){
								app.$.canvas.cells[i].className = app.brushes.current[0].className
								app.$.canvas.cells[i].setAttribute('color-byte',app.brushes.current[0].color)
							}
						}

						app.history.add()
						app.preview.update()
					}
			},
		handler: function(action)
			{
				return function(e)
				{
					e.preventDefault
					app.toolbar.actions[action]()
				}
			},
		act: function(obj,action)
			{
				obj.addEventListener('click',app.toolbar.handler(action))
			},
		tools: 
			{
				current: 'brush',
				select: function(tool)
					{
						app.canvas.selection.hide()
						app.canvas.selection.moving = false
						app.canvas.selection.start = [0,0]
						app.canvas.selection.end = [0,0]

						app.$.canvas.style.cursor = ''

						;(tool !== 'select') && (app.$.toolbar.clear.childNodes[1].textContent = 'Clear screen')


						app.toolbar.tools.current = tool
						app.$.canvas.className = app.$.canvas.className.replace(/ tool-[a-z]+/g,' tool-' + tool)
					},
				handler: function(key)
					{
						app.$.tools[key].addEventListener('click',function(e){
							app.toolbar.tools.select(key)
						})
					}
			},
		dropdowns: 
			{
				handler: function(e)
					{
						e.preventDefault()
						e.stopPropagation()
						app.toolbar.dropdowns.closeAll()
						;(this.className.indexOf('opened') > -1) ? this.className = this.className.replace(' opened','') : this.className += ' opened'
					},
				closeAll: function()
					{
						var j

						for (j = 0; j < app.$.dropdowns.length; j++){
							app.$.dropdowns[j].className = app.$.dropdowns[j].className.replace(' opened','')
						}
					},
				init: function()
					{
						var i

						for (i = 0; i < app.$.dropdowns.length; i++){
							app.$.dropdowns[i].addEventListener('click',app.toolbar.dropdowns.handler)
						}

						document.addEventListener('click',function(e){
							app.toolbar.dropdowns.closeAll()
						})
					}
			},
		init: function()
			{
				var key

				for (key in app.$.toolbar){
					app.toolbar.act(app.$.toolbar[key],key)
				}

				for (key in app.$.tools){
					app.toolbar.tools.handler(key)
				}

				app.$.paletteSelector.addEventListener('change',function(e){
					app.$.cssPalette.href = '/assets/styles/palette/' + this.value + '.css'
					app.preview.update()
					app.storage.settings.save()
				})

				app.toolbar.dropdowns.init()
			}
	}

	app.preview = {
		context: null,
		update: function()
			{
    			var imageData = app.preview.context.getImageData(0, 0, app.settings.size.width, app.settings.size.height),
    				buf = new ArrayBuffer(imageData.data.length),
				    buf8 = new Uint8ClampedArray(buf),
				    data = new Uint32Array(buf),
				    x,y,rgb, index

				    for (y = 0; y < app.settings.size.height; y++){
				    	for (x = 0; x < app.settings.size.width; x++){
				    		index = y * app.settings.size.width + x
				    		rgb = window
				    			.getComputedStyle(app.$.canvas.cells[index], null)
				    				.getPropertyValue( 'background-color' )
				    					.match(/[0-9]+/g)
					    	rgb = {
					    		r: rgb[0],
					    		g: rgb[1],
					    		b: rgb[2]
					    	}

					    	data[index] = 
					    		(255 << 24)   |    // alpha
				                (rgb.b << 16) |    // blue
				                (rgb.g <<  8) |    // green
				                rgb.r    
				    	}
				    }

				    imageData.data.set(buf8);
				    app.preview.context.putImageData(imageData, 0, 0);
			},
		zoom: function(e)
			{
				var min = 1,
					max = 8,
					current = Math.floor(parseInt(app.$.preview.style.width || app.$.preview.getAttribute('width')) / app.settings.size.width)

				e && e.preventDefault()

				current = current * 2
				;(current > max) && (current = min)

				app.$.preview.style.width = app.settings.size.width * current + 'px'
				app.$.preview.style.height = app.settings.size.height * current + 'px'
				app.storage.settings.save()
			},
		init: function()
			{
				app.preview.context = app.$.preview.getContext('2d')
				app.preview.context.imageSmoothingEnabled = false
				app.$.preview.addEventListener('click', app.preview.zoom)
			}
	}

	app.canvas = {
		state: false,
		lock: false,
		previous: null,
		selection: 
			{
				start: [0,0],
				end: [0,0],
				catching: false,
				moving: false,
				clone: null,
				put: [0,0],
				flush: function()
					{
						app.canvas.selection.hide()
						app.canvas.selection.start = [0,0]
						app.canvas.selection.end = [0,0]
					},
				s: function()
					{
						return [
							Math.min(app.canvas.selection.start[0],app.canvas.selection.end[0]),
							Math.min(app.canvas.selection.start[1],app.canvas.selection.end[1])
					    ]
					},
				e: function()
					{
						return [
							Math.max(app.canvas.selection.start[0],app.canvas.selection.end[0]),
							Math.max(app.canvas.selection.start[1],app.canvas.selection.end[1])
						]
					},
				has: function(x,y)
					{
						var s = app.canvas.selection.s(),e = app.canvas.selection.e()

						if (x instanceof HTMLElement){
							y = x.parentNode.rowIndex
							x = x.cellIndex
						}

						return (x >= s[0] && x <= e[0] && y >= s[1] && y <= e[1])
					},
				setStart: function(td)
					{
						app.canvas.selection.start = [
							td.cellIndex,
							td.parentNode.rowIndex
						]
					},
				setEnd: function(td)
					{
						app.canvas.selection.end = [
							td.cellIndex,
							td.parentNode.rowIndex
						]
					},
				move: function(ox,oy)
					{
						var clone = app.canvas.selection.clone,
							s = app.canvas.selection.s(),
							e = app.canvas.selection.e(),
							w = e[0] - s[0] + 1,
							h = e[1] - s[1] + 1,
							x,y,tr,td, orig

						app.canvas.loop(function(x,y,cell){
								cell.style.opacity = 1
							if (app.canvas.selection.has(x,y)){
								cell.style.opacity = 0.5
							}
						})

						if (clone === null){
							clone = document.createElement('table')
							clone.style.tableLayout = 'fixed'
							clone.style.position = 'absolute'
							clone.style.left = 0
							clone.style.top = 0
							clone.style.border = '2px dashed #fff'
							clone.style.pointerEvents = 'none'

							document.body.appendChild(clone)
							app.canvas.selection.clone = clone
						} else {
							app.canvas.selection.clone.innerHTML = ''
							app.canvas.selection.clone.style.display = 'table'
						}

							for (y = 0; y < h; y++){
								tr = document.createElement('tr')

								for (x = 0; x < w; x++){
									orig = app.$.canvas.querySelectorAll('tr')[s[1] + y].querySelectorAll('td')[s[0] + x]

									td = document.createElement('td')
									td.style.width = '16px'
									td.style.height = '16px'
									td.className = orig.className
									td.setAttribute('color-byte',orig.getAttribute('color-byte'))

									tr.appendChild(td)
								}

								clone.appendChild(tr)
							}

						ox = Math.max
								(
									app.$.canvas.offsetLeft, 
									Math.min
										(
											app.$.canvas.offsetLeft + app.$.canvas.clientWidth - app.canvas.selection.clone.clientWidth,
											Math.floor(ox / 16) * 16 + (app.$.canvas.offsetLeft - Math.floor(app.$.canvas.offsetLeft / 16) * 16) - 1
										)
								)
						oy = Math.max
								(
									app.$.canvas.offsetTop, 
									Math.min
										(
											app.$.canvas.offsetTop + app.$.canvas.clientHeight - app.canvas.selection.clone.clientHeight,										
											Math.floor(oy / 16) * 16 + (app.$.canvas.offsetTop - Math.floor(app.$.canvas.offsetTop / 16) * 16) - 1
										)
								)

						app.canvas.selection.put = [
							Math.ceil((ox - app.$.canvas.offsetLeft) / 16),
							Math.ceil((oy - app.$.canvas.offsetTop) / 16)
						]

						clone.style.left = ox + "px"
						clone.style.top = oy + 'px'
					},
				apply: function(copy, brush)
					{
						var tableClone = [],
							sx = app.canvas.selection.put[0],
							sy = app.canvas.selection.put[1],
							ctds = app.canvas.selection.clone.querySelectorAll('td'),
							sh = app.canvas.selection.clone.querySelectorAll('tr').length,
							sw = ctds.length / sh,
							color, cn,
							x,y

						copy = copy || false
						brush = brush || 0

						function isin(x,y){ return (x >= sx && x <= sx + sw - 1 && y >=sy && y <= sy + sh - 1) }

						for (y = 0; y < app.settings.size.height; y++){
							for (x = 0; x < app.settings.size.width; x++){
								if (isin(x,y)){
									color = ctds[(y - sy) * sw + (x - sx)].getAttribute('color-byte')
									cn = ctds[(y - sy) * sw + (x - sx)].className
								} else if (!copy && app.canvas.selection.has(x,y)){
									color = app.brushes.current[brush].color 
									cn = app.brushes.current[brush].className
								} else {
									color = app.$.canvas.cells[y * app.settings.size.width + x].getAttribute('color-byte')
									cn = app.$.canvas.cells[y * app.settings.size.width + x].className
								}

								tableClone.push({
									color: color,
									className: cn
								})
							}
						}

						app.canvas.selection.start = app.canvas.selection.put
						app.canvas.selection.end = [
							sx + sw - 1,
							sy + sh - 1
						]
						app.canvas.selection.show()

						app.canvas.loop(function(x,y,cell,index){
							cell.style.opacity = 1
							cell.setAttribute('color-byte',tableClone[index].color)
							cell.className = tableClone[index].className
						})

						app.history.add()
						app.storage.draft.save()
						app.preview.update()
					},
				hide: function()
					{
						app.canvas.loop(function(x,y,cell){
							cell.style.borderLeft = 'none'
							cell.style.borderTop = 'none'
							cell.style.borderRight = 'none'
							cell.style.borderBottom = 'none'
						})
					},
				show: function()
					{
						var s = app.canvas.selection.s(),e = app.canvas.selection.e()

						app.canvas.selection.hide()

						app.canvas.loop(function(x,y,cell){
							if (x == s[0] && y >= s[1] && y <= e[1]){
								cell.style.borderLeft = '2px dashed #fff'
							}
							if (y == s[1] && x >= s[0] && x <= e[0]){
								cell.style.borderTop = '2px dashed #fff'
							}

							if (x == e[0] && y <= e[1] && y >= s[1]) {
								cell.style.borderRight = '2px dashed #fff'
							}

							if (y == e[1] && x <= e[0] && x >= s[0]){
								cell.style.borderBottom = '2px dashed #fff'
							}
						})
					}
			},
		draw:
			{
				on: function(e)
					{
						app.canvas.state = true

						if (e.shiftKey) app.canvas.lock = [
							e.target.cellIndex,
							e.target.parentNode.rowIndex
						]

						;(!app.canvas.selection.has(e.target)) && (app.canvas.selection.setStart(e.target))
					},
				off: function(e)
					{
						if (app.canvas.state) app.storage.draft.save()

						;(app.toolbar.tools.current === 'select')
							? app.$.toolbar.clear.childNodes[1].textContent = 'Fill selection'
							: app.$.toolbar.clear.childNodes[1].textContent = 'Clear screen'

						app.canvas.state = false
						app.canvas.lock = false
						app.canvas.selection.catching = false
						app.canvas.previous = null

						if (app.canvas.selection.moving && app.toolbar.tools.current === 'select'){
							app.canvas.selection.apply(e.ctrlKey,e.button)
							app.canvas.selection.moving = false
						}

						app.canvas.selection.clone && (app.canvas.selection.clone.style.display = 'none')
					}
			},
		loop: function(handler, type, def)
			{
				var x,y,cell,index,
					res

				switch (type){
					case 'array':
					case 'indexarray':
						res = def || []
						break;
					case 'number':
						res = def || 0
						break;
					case 'string':
					default:
						res = def || ''
						break;
				}

				for (y = 0; y < app.settings.size.height; y++){
					for (x = 0; x < app.settings.size.width; x++){
						index = y * app.settings.size.width + x
						cell = app.$.canvas.cells[index]

						switch (type){
							case 'array':
								;(res.push)
									? res.push(handler(x,y,cell,index))
									: res[index] = handler(x,y,cell,index)
								break;
							case 'indexarray':
								res[index] = handler(x,y,cell,index)
							default:
								res += handler(x,y,cell,index)
						}
					}
				}

				return res
			},
		paint: function(e)
			{
				e.preventDefault()

				app.info.position(this.cellIndex,this.parentNode.rowIndex)

				if (e.shiftKey && app.canvas.state && !app.canvas.lock){
					app.canvas.lock = [
						this.cellIndex,
						this.parentNode.rowIndex
					]
				}

				if (app.canvas.state && this === e.target){
					app.canvas.put.call(this,e)
				} else if (app.toolbar.tools.current === 'select') {
					;(app.canvas.selection.has(this.cellIndex,this.parentNode.rowIndex))
						? app.$.canvas.style.cursor = 'move'
						: app.$.canvas.style.cursor = 'crosshair'
				}
			},
		touch: function(e)
			{
				if (!app.canvas.selection.has(this) && !app.canvas.selection.moving){
					app.canvas.selection.setStart(e.target)
					app.canvas.selection.setEnd(e.target)
					app.canvas.selection.catching = true
				} else {
					ss = app.$.canvas.querySelectorAll('tr')[app.canvas.selection.s()[1]].querySelectorAll('td')[app.canvas.selection.s()[0]]
					app.canvas.selection.catching = false
					app.canvas.selection.moving = true
					app.canvas.selection.offsetX = e.clientX - ss.getBoundingClientRect().left
					app.canvas.selection.offsetY = e.clientY - ss.getBoundingClientRect().top
				}

				if (e.shiftKey){
					app.canvas.lock = [
						this.cellIndex,
						this.parentNode.rowIndex
					]
				}

				app.canvas.put.apply(this,arguments)
			},
		put: function(e)
			{
				var btn = [null,0,2,null,1],
					cx,cy,targetCell

				btn = btn[e.buttons] || 0

				e.preventDefault()
				switch (app.toolbar.tools.current){
					case 'select':
						if (!app.canvas.selection.moving && (!app.canvas.selection.has(this) || app.canvas.selection.catching)){
							app.canvas.selection.end = [e.target.cellIndex,e.target.parentNode.rowIndex]
						} else if (app.canvas.selection.moving) {
							app.canvas.selection.move(e.clientX - app.canvas.selection.offsetX, e.clientY - app.canvas.selection.offsetY)
						}
						app.canvas.selection.show()
						break;
					case 'brush':
					default:
						switch (app.canvas.mode.current){
							case 'pickup':
								app.brushes.select(btn,app.palette.search(this.className))
								break;
							case 'def':
							default:
								if (app.canvas.lock){
									if (!app.canvas.lock[3]){
										cx = Math.abs(Math.floor(e.movementX / 2))
										cy = Math.abs(Math.floor(e.movementY / 2))


										if (cx !== 0 || cy !==0){
											app.canvas.lock[3] = (cx > cy) ? 'x' : 'y'
										}
									}

									cx = app.canvas.lock[0]
									cy = app.canvas.lock[1]

									if (cx == this.cellIndex && this.parentNode.rowIndex == cy){
										this.className = app.brushes.current[btn].className
										this.setAttribute('color-byte',app.brushes.current[btn].color)
									} else {
										if (app.canvas.lock[3]){
											targetCell = (app.canvas.lock[3] === 'x')
												? app.$.canvas.cells[cy * app.settings.size.width + this.cellIndex]
												: app.$.canvas.cells[this.parentNode.rowIndex * app.settings.size.width + cx]

											app.canvas.line(btn, targetCell)
										}
									}
								} else {
									app.canvas.line(btn, this)
								}
								app.preview.update()

								break;
						}
						break;
				}
			},
		line: function(btn, target)
			{
				var x2 = target.cellIndex,
					y2 = target.parentNode.rowIndex
					
				;(app.canvas.previous === null) && (app.canvas.previous = [x2,y2])

				var x1 = app.canvas.previous[0],
					y1 = app.canvas.previous[1],
					deltaX = Math.abs(x2 - x1),
    				deltaY = Math.abs(y2 - y1),
    				signX = x1 < x2 ? 1 : -1,
    				signY = y1 < y2 ? 1 : -1,
    				error = deltaX - deltaY,
    				error2

    				app.canvas.pixel(x2, y2, btn)

					while (x1 != x2 || y1 != y2) 
					{
						app.canvas.pixel(x1, y1, btn)
						error2 = error * 2;

						if (error2 > -deltaY) 
							{
							    error -= deltaY;
							    x1 += signX;
							}

						if (error2 < deltaX) 
							{
							    error += deltaX;
							    y1 += signY;
							}
					}

				app.canvas.previous = [x2,y2]
			},
		pixel: function(x,y,btn)
			{
				cell = app.$.canvas.cells[y * app.settings.size.width + x]
				cell.className = app.brushes.current[btn].className
				cell.setAttribute('color-byte',app.brushes.current[btn].color)
			},
		setColor: function(x,y,color)
			{
				var td = app.$.canvas.querySelectorAll('tr')[y].querySelectorAll('td')[x]

				td.setAttribute('color-byte',color)
				td.className = app.palette.classByColor(color)
			},
		applyColor: function(x,y,color)
			{
				app.canvas.setColor(x,y,color.color)
			},
		getColor: function(x,y)
			{
				return app.$.canvas.querySelectorAll('tr')[y].querySelectorAll('td')[x].getAttribute('color-byte') || 0
			},
		mode:
			{
				current: 'def',
				def: function()
					{
						app.canvas.mode.current = 'def'

						app.$.canvas.className = app.$.canvas.className
							.replace(/ color-pickup/g, '')
					},
				pickup: function()
					{
						app.canvas.mode.current = 'pickup'
						;(app.$.canvas.className.indexOf('color-pickup') < 0) && (app.$.canvas.className += ' color-pickup')
					},
			},
		hotkeys: function(e)
			{
				if (e.ctrlKey || e.altKey){
					app.canvas.mode.pickup()
				}
			},
		history: function(e)
			{
				(app.canvas.state) && (app.history.add())
			},
		init: function()
			{
				var i, ss

				app.$.canvas.oncontextmenu = function(){ return false }
				app.$.canvas.addEventListener('mousedown',app.canvas.draw.on)
				document.body.addEventListener('mouseup',app.canvas.history)
				document.body.addEventListener('mouseup',app.canvas.draw.off)

				for (i = 0; i < app.$.canvas.cells.length; i++){
					app.$.canvas.cells[i].addEventListener('mousemove',app.canvas.paint)
					app.$.canvas.cells[i].addEventListener('mousedown',app.canvas.touch)
				}

				document.querySelector('.workspace').addEventListener('click',function(e){
					if (e.target === this){
						app.canvas.selection.hide()
						app.canvas.selection.start = [0,0]
						app.canvas.selection.end = [0,0]
						app.$.toolbar.clear.childNodes[1].textContent = 'Clear screen'
					}
				})

				document.addEventListener('keydown',app.canvas.hotkeys)
				document.addEventListener('keyup',app.canvas.mode.def)
			}
	}

	app.mode = {
		hideColors: true,
		color: function()
			{
				return document.body.className.indexOf('mode-color') > -1
			},
		chunky: function()
			{
				return document.body.className.indexOf('mode-chunky') > -1
			},
		current: function()
			{
				return (app.mode.color())
					? 'color'
					: 'chunky'
			},
		set: function(mode)
			{
				var on = (mode === 'chunky')

				app.mode.trigger(on)
				app.$.mode.checked = on
			},
		trigger: function(on)
			{
				document.body.className = on ? 'mode-chunky' : 'mode-color'
				return on
			},
		handler: function(e)
			{
				e.preventDefault()

				app.mode.trigger(this.checked)
				app.storage.settings.save()
			},
		grid:
			{
				snap: false,
				handler: function(e)
					{
						e.preventDefault()

						app.mode.grid.trigger(this.checked)
						app.storage.settings.save()
					},
				on: function()
					{
						return app.$.canvas.className.indexOf('grid') > -1
					},
				snaph: function(e)
					{
						e.preventDefault
						app.mode.grid.snap = this.checked
						app.storage.settings.save()
						app.mode.grid.trigger(app.mode.grid.on())
					},
				trigger: function(on)
					{
						var snap = (app.mode.grid.snap) ? ' snap' : ''

						app.$.canvas.className = app.$.canvas.className
							.replace(/ grid/g,'')
							.replace(/ snap/g,'')

						;(on)
							? app.$.canvas.className += ' grid' + snap
							: app.$.canvas.className += snap
					}
			},
		palette: function(on)
			{
				app.mode.hideColors = on
				;(!on)
					? app.$.palette.className += ' all-colors'
					: app.$.palette.className = app.$.palette.className.replace(' all-colors','')

				app.palette.fill()
				app.palette.sort[app.palette.sort.mode]()
				app.palette.redraw()
				app.storage.settings.save()
			},
		init: function()
			{
				app.$.mode.addEventListener('change',app.mode.handler)
				app.$.grid.addEventListener('change',app.mode.grid.handler)
				app.$.snap.addEventListener('change',app.mode.grid.snaph)
				app.$.hideColors.addEventListener('change',function(e){
					app.mode.palette(this.checked)
				})
			}
	}

	app.brushes = {
		current: [],
		update: function()
			{
				var i

				for (i=0; i < app.$.brushes.length; i++){
					;(app.brushes.current[i])
						? app.$.brushes[i].className = app.$.brushes[i].className.replace(/ c\-[0-9]x[0-9]( bright)?/,app.brushes.current[i].className)
						: app.$.brushes[i].className = app.$.brushes[i].className.replace(/ c\-[0-9]x[0-9]( bright)?/,' c-0x0')
				}

				app.$.toolbar.clearIndicator.style.backgroundColor = app.brushes.current[0].css
			},
		select: function(button,colorData)
			{
				app.brushes.current[button] = colorData
				app.brushes.update()
			},
		init: function()
			{
				app.brushes.current[0] = app.palette.data(0,0,0)
				app.brushes.current[1] = app.palette.data(0,0,0)
				app.brushes.current[2] = app.palette.data(0,0,0)
			}
	}

	app.palette = {
		SPECTRUM:
			[
				/* nonbright */
				[
					[0,0,0],[0,0,192],[192,0,0],[192,0,192],[0,192,0],[0,192,192],[192,192,0],[192,192,192]
				],
				/* bright */
				[
					[0,0,0],[0,0,255],[255,0,0],[255,0,255],[0,255,0],[0,255,255],[255,255,0],[255,255,255]
				]
			],
		SOLID: [0,63,127,18,82,54,118,36,100,45,109,9,73,27,91],
		HALFTONE: [],
		FULL: [],
		current: [],
		rgb: function(paper, ink, bright)
			{
				var c = {
					r: (app.palette.SPECTRUM[bright][paper][0] + app.palette.SPECTRUM[bright][ink][0]) >> 1,
					g: (app.palette.SPECTRUM[bright][paper][1] + app.palette.SPECTRUM[bright][ink][1]) >> 1,
					b: (app.palette.SPECTRUM[bright][paper][2] + app.palette.SPECTRUM[bright][ink][2]) >> 1
				}

				return c
			},
		hue: function(rgb)
			{
				var r = rgb.r / 255,
					g = rgb.g / 255,
					b = rgb.b / 255,
					max = Math.max(r, Math.max(g,b)),
					min = Math.min(r, Math.min(g,b)),
					hue = -1

				if (r === g && g === b) return hue
				if (r >= g && r >= b) hue = (g - b) / (max - min) * 60
				if (g >= r && g >= b) hue = (2.0 + (b - r) / (max - min)) * 60
				if (b >= r && b >= g) hue = (4.0 + (r - g) / (max - min)) * 60

				if (hue < 0) hue = hue + 360

				return hue
			},
		saturation: function(rgb)
			{
				var l = app.palette.luminance(rgb),
					max = Math.max(rgb.r, Math.max(rgb.g, rgb.b)),
					min = Math.min(rgb.r, Math.min(rgb.g, rgb.b))

				return (l < 0.5)
					? (max - min) / (max + min)
					: (max - min) / (2.0 - max - min)
			},
		luminance: function(rgb)
			{
				var max = Math.max(rgb.r, Math.max(rgb.g, rgb.b)),
					min = Math.min(rgb.r, Math.min(rgb.g, rgb.b))

				return Math.floor((min + max)/2)
			},
		hex: function(rgb)
			{
				return app.palette.hexb(rgb.b) + app.palette.hexb(rgb.g) + app.palette.hexb(rgb.r) + app.palette.hexb(0)
			},
		hexb: function(byte)
			{
				var hexDigits = '0123456789abcdef'
				return '%' + hexDigits.substr(byte >> 4,1) + hexDigits.substr(byte & 0x0f,1)			
			},
		css: function(rgb)
			{
				return '#' + (app.palette.hexb(rgb.r) + app.palette.hexb(rgb.g) + app.palette.hexb(rgb.b)).replace(/%/g,'')
			},
		isSolid: function(color)
			{
				return (app.palette.SOLID.indexOf(parseInt(color)) > -1)
			},
		className: function(paper, ink, bright)
			{
				return (bright)
					? ' c-' + paper + 'x' + ink + ' bright'
					: ' c-' + paper + 'x' + ink 
			},
		search: function(className)
			{
				var i, found = 0,
					color, bright

				bright = (className.indexOf('bright') > -1) ? 1 : 0
				color = className.match(/c-[0-9]x[0-9]/g)

				;(color)
					? color = color[0].replace('c-','').split('x')
					: color = [0,0]

				color = app.palette.color(color[0],color[1],bright)

				for (i = 0; i < app.palette.HALFTONE.length; i++){
					if (app.palette.HALFTONE[i].color === color){
						found = i
					}
				}

				return app.palette.HALFTONE[found]
			},
		rgbByColor: function(color)
			{
				var i, found = 0

				for (i = 0; i < app.palette.FULL.length; i++){
					if (app.palette.FULL[i].color === parseInt(color)){
						found = i
					}
				}

				return app.palette.FULL[found].rgb
			},
		classByColor: function(color)
			{
				var i, found = 0

				for (i = 0; i < app.palette.HALFTONE.length; i++){
					if (app.palette.HALFTONE[i].color === parseInt(color)){
						found = i
					}
				}

				return app.palette.HALFTONE[found].className
			},
		color: function (paper, ink, bright)
			{
				return (ink | paper << 3 | bright << 6)
			},
		data: function(paper, ink, bright)
			{
				var rgb = app.palette.rgb(paper, ink, bright)
				
				return {
					paper: paper,
					ink: ink,
					bright: bright,
					color: app.palette.color(paper,ink,bright),
					rgb: rgb,
					css: app.palette.css(rgb),
					className: app.palette.className(paper, ink, bright),
					hex: app.palette.hex(rgb),
					hue: app.palette.hue(rgb),
					luminance: app.palette.luminance(rgb),
					saturation: app.palette.saturation(rgb),
					brightness: 0.2126 * rgb.r + 0.7152 * rgb.g + 0.0722 * rgb.b
				}
			},
		cells:
			{
				handler: function(e)
					{
						e.preventDefault()
						//0 - left, 1 - center, 2 - right
						app.brushes.select(e.button,this.colorData)
						app.storage.settings.save()
					},
				clear: function()
					{
						app.$.palette.innerHTML = ''
					},
				add: function(colorData)
					{
						if (colorData.color !== 64){
							var cell = document.createElement('li')

							cell.className = 'cell color ' + colorData.className
							cell.setAttribute('luminance',colorData.luminance)
							cell.setAttribute('hue',colorData.hue)
							cell.setAttribute('rgb','(' + colorData.rgb.r + ',' + colorData.rgb.g + ',' + colorData.rgb.b + ')')
							cell.colorData = colorData
							cell.oncontextmenu = function() { return false }
							cell.addEventListener('mousedown',app.palette.cells.handler)

							app.$.palette.appendChild(cell)
						}
					}
			},
		sort: 
			{
				reverse: false,
				update: function()
					{
						var sort = document.querySelector('.app-sort-trigger:checked').value

						app.palette.sort.mode = sort
						app.palette.sort[sort]()
						app.palette.redraw()
					},
				handler: function()
					{
						app.palette.sort.update()
						app.storage.settings.save()
					},
				color: function()
					{
						app.palette.HALFTONE.sort(function(a,b){
							return (app.palette.sort.reverse)
								? a.color - b.color
								: b.color - a.color
						})
					},
				hue: function()
					{
						app.palette.HALFTONE.sort(function(a,b) {
							return (app.palette.sort.reverse)
								? (a.hue - b.hue) * 1000 + (a.luminance - b.luminance)
								: (b.hue - a.hue) * 1000 + (b.luminance - a.luminance)
						})
					},
				rgb: function()
					{
						app.palette.HALFTONE.sort(function(a,b){
							return (app.palette.sort.reverse)
								? (a.rgb.r - b.rgb.r) * 100 + (a.rgb.g - b.rgb.g) * 10 + (a.rgb.b - b.rgb.b)
								: (b.rgb.r - a.rgb.r) * 100 + (b.rgb.g - a.rgb.g) * 10 + (b.rgb.b - a.rgb.b)
						})
					},
				init: function()
					{
						var i

						for (i = 0; i < app.$.sort.length; i++){
							app.$.sort[i].addEventListener('change',app.palette.sort.handler)
						}
					}
			},
		redraw: function()
			{
				var i,
					color = null

				app.palette.cells.clear()
				for (i = 0; i < app.palette.HALFTONE.length; i++){
					color = app.palette.HALFTONE[i]
					app.palette.cells.add(color)
				}

				app.$.palette.setAttribute('sort',app.palette.sort.mode)
				;(app.palette.sort.reverse)
					? app.$.palette.setAttribute('reverse','reverse')
					: app.$.palette.removeAttribute('reverse')
			},
		fill: function()
			{
				var paper, ink, bright, 
					colorData,
					antid = []

				app.palette.HALFTONE = []
				app.palette.FULL = []

				for (bright = 0; bright < 2; bright++){
					for (paper = 0; paper < 8; paper++){
						for (ink = paper; ink < 8; ink++){
							colorData = app.palette.data(paper, ink, bright)
							colorData.index = app.palette.HALFTONE.length

							if (!app.mode.hideColors || antid.indexOf(colorData.css) < 0){
								app.palette.HALFTONE.push(colorData)
								antid.push(colorData.css)
							}

							app.palette.FULL.push(colorData)
						}
					}
				}
			},
		init: function()
			{
				app.palette.fill()

				app.$.reverse.addEventListener('change',function(e){
					e.preventDefault
					app.palette.sort.reverse = this.checked
					app.storage.settings.save()
					app.palette.sort.update()
				})

				app.palette.sort.init()
				app.palette.sort.mode = 'rgb'
				app.palette.sort.rgb()
				app.palette.redraw()
			}
	}

	/* ONLOAD */

	app.polyfills()

	app.dom.init()
	app.toolbar.init()
	app.windows.init()
	app.dialog.init()

	app.mode.init()
	app.brushes.init()
	app.palette.init()

	app.preview.init()
	app.canvas.init()

	app.history.init()

	app.storage.settings.load()

	app.rorg.init()

})()